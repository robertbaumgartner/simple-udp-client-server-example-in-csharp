﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using static System.Console;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var client = new UdpClient())
            {
                try
                {
                    client.Connect(new IPEndPoint(IPAddress.Loopback, 5555));
                    var r = new Random();
                    Write("Enter Client ID: ");
                    var id = ReadLine();
                    while (true)
                    {
                        var date = DateTime.Now.ToLocalTime().ToString("hh:mm:ss");
                        var buffer = Encoding.Default.GetBytes($"ID: {id}  {date}");
                        client.Send(buffer, buffer.Length);
                        Thread.Sleep(r.Next(400, 2000));
                    }
                }
                catch (Exception e)
                {
                    WriteLine(e.Message);
                }
            }
        }
    }
}

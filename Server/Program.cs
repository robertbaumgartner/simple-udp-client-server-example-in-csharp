﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using static System.Console;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Title = "Server";
            try
            {
                using (var server = new UdpClient(new IPEndPoint(IPAddress.Any, 5555)))
                {
                    while (true)
                    {
                        var remoteEndp = new IPEndPoint(IPAddress.Any, 0);
                        var buffer = server.Receive(ref remoteEndp);
                    
                        var data = Encoding.Default.GetString(buffer);
                        WriteLine("Receiving data from {0}: {1}", remoteEndp, data);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message);
            }
        }
    }
}
